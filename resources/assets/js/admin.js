(function($) {
    $(function() {
        var userTabs = $('.user-group-tabs li'),
            userPanels = $('.panel');

        var openTabPanel = function(elem) {
            var tabPanel = elem.data("tab");

            userTabs.removeClass('active').addClass('inactive');
            elem.removeClass('inactive').addClass('active');

            userPanels.each(function() {
               if($(this).data('panel') === tabPanel) {
                   userPanels.removeClass('active');
                   $(this).addClass('active');
               }
            });

        };

        // Event Listeners
        userTabs.each(function() {
            $(this).on('click', function() {
                openTabPanel($(this));
            });
        });

    });
})(jQuery);