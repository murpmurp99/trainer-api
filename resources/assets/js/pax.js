(function($) {
    $(function() {

        var datePicker = $('#date_select');
        var modalActions = $('.modal_actions li');
        var modals = $('.modal_views div');
        var action = $('#submit_action_today');
        var message = $('#message_body');
        var date = "";

        if(datePicker !== null) {
            datePicker.pickadate({
                onSet: function(context) {
                    date = context;
                }
            })
        }

        if(modalActions !== null) {
            modalActions.each(function() {
                $(this).on('click', function() {

                    var data = $(this).data('view');

                    modalActions.removeClass('active');

                    if(!$(this).hasClass('active')) {
                        $(this).addClass('active');
                    }

                    modals.each(function() {
                        if($(this).data('view') === data) {

                            modals.removeClass('active');

                            if(!$(this).hasClass('active')) {
                                $(this).addClass('active');
                            }
                        }
                    });

                });
            });
        }

        action.on('click', function() {

            var jsonBody = {
                "postType": "1",
                "date": date.select,
                "body": message.val()
            };

            if(date.select === "") {
                datePicker.css({
                    border: "1px solid red"
                });
            } else if(message.val() === "") {
                message.css({
                    border: "1px solid red"
                })
            }

            submitForm(jsonBody);
        });


        function submitForm(data) {
            $.post('/rest/api/v1', function(data) {

            });
        }

    });
})(jQuery);