<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ActiveX Admin</title>

    <script src="//use.typekit.net/aiv5scx.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>

	<link href="https://fontastic.s3.amazonaws.com/pwZEjLVJ7d2CiQYfQ5BWjW/icons.css" rel="stylesheet">
	<link href="{{ elixir("css/admin.css") }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

</head>
<body>

	@yield('content')
	@yield('popups')

	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js"></script>
	<script src="{{ elixir("js/all.js") }}"></script>
</body>
</html>
