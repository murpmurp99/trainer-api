@extends('app')

@section('content')
    <section class="admin_container">
        @include('components.navigation')
        @include('components.users-list')
    </section>
@endsection