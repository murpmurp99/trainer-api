@extends('app')

@section('content')
    <section class="admin_container">
        @include('components.navigation')
        @include('components.pax-list')
    </section>

    @include('modals.pax-modal')
@endsection