<div class="bg_overlay"></div>
<div class="modal_overlay">
    <div class="modal_container">
        <div class="modal_header">
            <h2>Pax: </h2>
        </div>
        <div class="modal_view_actions">
            <ul class="modal_actions">
                <li class="modal_action" data-view="message_stream">
                    <h4>Message Stream</h4>
                </li>
                <li class="modal_action active" data-view="new_message">
                    <h4>Schedule New Message</h4>
                </li>
            </ul>
        </div>
        <div class="modal_views clearfix">
            <div class="message_stream" data-view="message_stream">

                <div class="no-data">
                    <p>This pax has no messages scheduled</p>
                </div>
            </div>
            <div class="new_message active" data-view="new_message">
                <label for="date_select">Select date to publish:</label>
                <input name="date_select" id="date_select" type="text" placeholder="select a date"/>

                <label for="photo_upload">Upload a photo:</label>
                <input name="pic_upload" type="file" id="photo_upload" accept="image/gif, image/jpeg, image/png, image/tiff">

                <label for="message_body" class="post_message">Post Message:</label>
                <textarea id="message_body" class="message" placeholder="What's your message..."></textarea>

                <a href="javascript:void(0);" id="submit_action_today" class="modal_submit_btn">Create Message</a>
                <a href="javascript:void(0);" id="submit_action_future" class="modal_submit_btn hidden">Schedule Message</a>
            </div>
        </div>
    </div>
</div>