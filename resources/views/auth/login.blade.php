@extends('app')

@section('content')
<section id="loginBody">

    <div class="leftBody withForm">
        <div class="outer center">

            <div class="logoContainer">
                <a class="logo xl" href="http://www.djove.com/purpose" data-icon="a"></a>
            </div>

            <div class="form-container sign-in" data-step="1">
                {!! Form::open(['url' => 'admin/login']) !!}
                    {!! $errors->first('email') !!}
                    {!! $errors->first('password') !!}

                    {!! Form::text('email', Input::old('email'), ['placeholder' => 'joe@joe.com', 'class' => 'inputElement center', 'id' => 'userEmail']) !!}
                    {!! Form::password('password', ['placeholder' => 'secret', 'class' => 'inputElement password center', 'id' => 'userPassword']) !!}

                    <label class="labelElement">Remember Me?</label>
                        <span class="checkbox">
                            <input type="checkbox">
                        <label data-on="&#x2714;" data-off="&#x2718;"></label>
                    </span>

                <div class="formAction">
                    {!! Form::submit('Sign In', ['id' => 'continueAction', 'class' => 'main-btn lrg']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="rightBody withDescription">
        <div class="outer soft-center">
            <h1>Working Made <span class="highlight">Easy</span>.</h1>
            <span class="definition"><i>eas·y</i></span>
            <p class="definitionExample">"achieved without great effort; presenting few difficulties."</p>

            <p class="bodyText">
                With Purpose ADAT <i>(Application Data Administration Tool)</i> it's not only simple to administer your data, but even simpler to create new and exciting data to engage your users.
                We built a tool that allows people to simply and efficiently scale their data, organize it, track it, and even trend it.
                We know that you care about your data, and we know that data comes in many shapes, sizes, and even colors. So why not build a tool that allows you to neatly, and effortlessly control it.
                So we as developers took it upon ourselves to create Purpose, which was made with one idea in mind. Every app has a "Purpose".
            </p>
        </div>
    </div>

</section>
@endsection
