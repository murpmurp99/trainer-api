<div class="content_container view_lists">
    <div class="list_header clearfix">
        <div class="list_left">
            <h2 class="list-header-h2">Users: <span class="small">{{ $users->total() }}</span></h2>
        </div>
        <div class="list_right">

        </div>
    </div>
    <div class="data_container">
        <div class="user_form-container user-specifics">
            <?php $i=1; ?>
            @foreach ($users as $user)
                <div class="settings_field user-ext clearfix @if($i == 1) first @elseif($i == count($users)) last @endif">
                    <a href="/admin/panel/user/{{ $user['id'] }}" class="user_link"></a>
                    <div class="user_detail_cont">
                        <div class="user_profile_picture">
                            <img src="{{ GravatarProfilePicture::get_gravatar($user['email']) }}" style="width: 35px;"/>
                        </div>
                        <h4 class="field-detail">{{ $user['first_name'] . " " . $user['last_name'] }} @if($user['admin'] == 1) <span class="admin-tag">Admin</span> @endif</h4>
                    </div>
                    <div class="user_quick-action">
                        <ul class="u_quick-actions">
                            <li>
                                <a href="javascript:void(0);" class="delete-btn icon-trash-bin"><span class="anim_mask"></span><p>Delete</p></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="delete-btn"><span class="anim_mask"></span><p>Disable</p></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php $i++; ?>
            @endforeach
        </div>

        {!! $users->render() !!}

        <div class="spacer"></div>
    </div>
</div>