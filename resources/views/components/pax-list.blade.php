<div class="content_container view_lists">
    <div class="list_header clearfix">
        <div class="list_left">
            <h2 class="list-header-h2">Pax: <span class="small">{{ $paxs->total() }}</span></h2>
        </div>
        <div class="list_right">

        </div>
    </div>
    <div class="data_container">
        <div class="pax_container">
            <?php $i=0; ?>
            @foreach($paxs as $pax)
                <div class="pax pax_{{ $pax->id }}" data-pax-id="{{ $pax->id }}">
                    <div class="pax_header">
                        <h2>{{ $pax->name }}  <i class="icon-close-circle-outline" id="delete_pax" data-pax-delete="{{ $pax->id }}"></i></h2>
                        <input type="text" value="{{ $pax->name }}" id="{{ $pax->name }}_pax_name" hidden/>
                        <div class="pax_admin">
                        @foreach($pax->paxUsers as $paxUser)
                            @if($paxUser->is_admin == 1)
                                <label>Creator:</label>
                                <div class="user_profile_picture">
                                    <img src="{{ GravatarProfilePicture::get_gravatar($paxUser->user->email) }}" style="width: 35px;"/>
                                </div>
                                <p class="pax_user_name">{{ $paxUser->user->first_name }} {{ $paxUser->user->last_name }}</p>

                                <div class="admin_actions">
                                    <i class="icon-message-talk" data-message-queue="{{ $pax->id }}"></i>
                                </div>
                            @endif
                        @endforeach
                        </div>

                        <div class="pax_description">
                            <p>{{ $pax->description }}</p>
                            <textarea id="{{ $pax->name }}_description" hidden>{{ $pax->description }}</textarea>
                        </div>
                    </div>
                    <div class="pax_tabs">
                        <ul class="pax_activators">
                            <li><a href="javascript:void(0);" class="pax_link">Activity</a></li>
                            <li class="active"><a href="javascript:void(0);" class="pax_link">Users</a></li>
                        </ul>
                    </div>

                    <div class="pax_detail_container">
                        <div class="user_pax_list active" data-link="users">
                            @foreach($pax->paxUsers as $paxUser)
                                <a href="/admin/panel/user/{{ $paxUser->user->id }}" class="user_container">
                                    <div class="user_profile_picture">
                                        <img src="{{ GravatarProfilePicture::get_gravatar($paxUser->user->email) }}" style="width: 35px;"/>
                                    </div>
                                    <p class="pax_user_name">{{ $paxUser->user->first_name }} {{ $paxUser->user->last_name }}</p>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <?php $i++; ?>
            @endforeach
        </div>
        <div class="spacer"></div>
        {{ $paxs->render() }}
    </div>
</div>