<aside class="admin_navigation">
    <div class="content_overflow">
        <a href="/admin/panel" class="user_info quick_tip active">
            <div class="user_image_clip">
                <div class="user_image">
                    <?php
                    $loginUser = Auth::user();
                    $email = $loginUser['email'];
                    ?>
                    <img src="{{ GravatarProfilePicture::get_gravatar($email) }}" style="width: 61px;"/>
                </div>
            </div>
            <h2>{{ $loginUser['first_name'] . " " . $loginUser['last_name'] }}</h2>
        </a>

        <div class="quick-action-container">
            <ul class="quick-actions">
                <li class="quick-action">
                    <a href="/admin/logout" class="action logout">
                        <i class="ico-one icon-logout"></i>
                    </a>
                </li>
                <li class="quick-action">
                    <a href="javascript:void(0);" class="action">
                        <i class="ico-one"></i>
                    </a>
                </li>
                <li class="quick-action">
                    <a href="javascript:void(0);" class="action">
                        <i class="ico-one"></i>
                    </a>
                </li>
            </ul>

            <div class="search-container">
                <input type="text" class="search-bar" placeholder="search..."/>
            </div>
        </div>

        <ul class="navigation_actions page_adds">
            <li>
                <a href="/admin/panel/users" class="action page_add">
                    <i class="icon-contacs"></i>
                    <p>Users</p>
                </a>
            </li>
            <li>
                <a href="/admin/panel/pax" class="action page_add">
                    <i class="ico-four icon-contact-group"></i>
                    <p>Paxs</p>
                </a>
            </li>
            <li>
                <a href="/admin/panel/knuxs" class="action page_add">
                    <i class="ico-five"></i>
                    <p>Knuxs</p>
                </a>
            </li>
            <li>
                <a href="/admin/panel/tabatas" class="action page_add">
                    <i class="ico-two"></i>
                    <p>Tabatas</p>
                </a>
            </li>
            <li>
                <a href="/admin/panel/exercises" class="action page_add">
                    <i class="ico-three"></i>
                    <p>Exercises</p>
                </a>
            </li>
        </ul>
    </div>
</aside>