<div class="content_container">
    <div class="user-header clearfix">
        <div class="left-side">
            <div class="user_profile_picture">
                <img src="{{ GravatarProfilePicture::get_gravatar($user['email']) }}" class="profile_picture" style="width: 100px;"/>
            </div>
            <h2 class="user_name">{{ $user['first_name'] . " " . $user['last_name'] }} <span class="knuxs">• Knuxs: 0</span></h2>
            <p class="email_address">{{ $user['email'] }}</p>
            @if($user['admin'] == 1)
                <span class="admin-tag">Admin</span>
            @endif
        </div>
        <div class="right-side">
            <div class="delete btn-cont">
                <a href="javascript:void(0);" class="delete-btn icon-trash-bin"><span class="anim_mask"></span><p>Delete</p></a>
            </div>
            <div class="save btn-cont">
                <a href="javascript:void(0);" class="save-btn">Update</a>
            </div>
        </div>
    </div>
    <ul class="user-group-tabs">
        <li class="tab active" data-tab="activity">
            <h3>Recent Activity</h3>
        </li>
        <li class="tab inactive" data-tab="pax">
            <h3>Pax</h3>
        </li>
        <li class="tab inactive" data-tab="custom_exercises">
            <h3>Custom Exercises</h3>
        </li>
        <li class="tab inactive" data-tab="tabata_workouts">
            <h3>Tabata Workouts</h3>
        </li>
        <li class="tab inactive" data-tab="events">
            <h3>Subscribed Events</h3>
        </li>
        <li class="tab inactive" data-tab="user_settings">
            <h3>User Settings</h3>
        </li>
    </ul>

    <div class="panel activity" data-panel="activity">
        @include('components.panels.user.activity-stream')
    </div>
    <div class="panel pax" data-panel="pax">
        @include('components.panels.user.paxs-steam')
    </div>
    <div class="panel custom_exercises" data-panel="custom_exercises">
        @include('components.panels.user.custom-exercises')
    </div>
    <div class="panel tabata_workouts" data-panel="tabata_workouts">
        @include('components.panels.user.tabata')
    </div>
    <div class="panel events" data-panel="events">
        @include('components.panels.user.events')
    </div>
    <div class="panel user_settings" data-panel="user_settings">
        @include('components.panels.user.settings')
    </div>
</div>