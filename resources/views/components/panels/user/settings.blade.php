<div class="settings_container">
    <div class="user_form-container">

        <div class="settings_field clearfix first">
            <h4 class="field-detail">Is User Active: </h4>
            <div class="box_container">
                <span class="identifier_label">NO</span>
                <span class="checkbox">
                    @if($user['active'] == 1)
                        <input type="checkbox" checked="checked">
                    @else
                        <input type="checkbox"/>
                    @endif
                    <label data-on="&#x2714;" data-off="&#x2718;"></label>
                </span>
                <span class="identifier_label">YES</span>
            </div>
        </div>

        <div class="settings_field clearfix multi">

        </div>

        <div class="settings_field clearfix">
            <h4 class="field-detail">Two Factor Authentication: </h4>
            <div class="box_container">
                <span class="identifier_label">NO</span>
                <span class="checkbox">
                    @if($user['two_factor'] == 1)
                        <input type="checkbox" checked="checked">
                    @else
                        <input type="checkbox"/>
                    @endif
                    <label data-on="&#x2714;" data-off="&#x2718;"></label>
                </span>
                <span class="identifier_label">YES</span>
            </div>
        </div>

        <div class="settings_field clearfix">
            <h4 class="field-detail">Administrator: </h4>
            <div class="box_container">
                <span class="identifier_label">NO</span>
                <span class="checkbox">
                    @if($user['admin'] == 1)
                        <input type="checkbox" checked="checked">
                    @else
                        <input type="checkbox"/>
                    @endif
                    <label data-on="&#x2714;" data-off="&#x2718;"></label>
                </span>
                <span class="identifier_label">YES</span>
            </div>
        </div>

        <div class="settings_field clearfix last">
            <h4 class="field-detail">Auto Remember Sign In: </h4>
            <div class="box_container">
                <span class="identifier_label">NO</span>
                <span class="checkbox">
                    @if(strlen($user['remember_token']) > 0)
                        <input type="checkbox" checked="checked">
                    @else
                        <input type="checkbox"/>
                    @endif
                    <label data-on="&#x2714;" data-off="&#x2718;"></label>
                </span>
                <span class="identifier_label">YES</span>
            </div>
        </div>
    </div>
</div>