<?php namespace App\Providers;

use App\Helpers\GravatarProfilePicture;
use Illuminate\Support\ServiceProvider;

class GravatarServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app['gravatarprofilepicture'] = $this->app->share(function($app)
		{
			return new GravatarProfilePicture();
		});

		// Shortcut so developers don't need to add an Alias in app/config/app.php
		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('gravatarprofilepicture', 'App\Helpers\GravatarProfilePicture');
		});
	}

}
