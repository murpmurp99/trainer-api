<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseCategories extends Model {

    protected $connection = 'mysql';
    protected $table = 'exercise_categories';
    protected $fillable = ['name'];

    public function exercises()
    {
        return $this->hasMany('App\Models\Exercises', 'id', 'exercise_category_id');
    }

}
