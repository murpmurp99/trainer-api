<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrainerPlanExercises extends Model {

    protected $connection = 'mysql';
    protected $table = 'trainer_plan_exercises';
//    protected $table = 'trainer_plan_exercises';

    public function exercise()
    {
        return $this->hasOne('App\Models\Exercises', 'id', 'exercise_id');
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\PlanAliases', 'training_plan_id', 'id');
    }

}
