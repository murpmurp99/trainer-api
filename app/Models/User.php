<?php namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use SoftDeletes, Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $connection = 'mysql';
    protected $table = 'users';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'aus_id',
        'facebook_id',
        'first_name',
        'last_name',
        'gender',
        'email_address',
        'birthday',
        'age',
        'weight',
        'height',
        'goal_type_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'deleted_at', 'google2fa_secret', 'remember_token'];

    /**
     * User has many goals
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userGoalType()
    {
        return $this->hasOne('App\Models\UserGoalTypes', 'id', 'goal_type_id');
    }

    public function pax()
    {
        return $this->belongsToMany('App\Models\PaxUsers', 'id', 'user_id');
    }

    public function getAuthPassword() {
        return $this->password;
    }



}
