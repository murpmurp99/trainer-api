<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model {

    protected $connection = 'trainer';
    protected $table = 'images';

    public function images() {
        return $this->belongsTo('App\PlanAliases');
    }

}
