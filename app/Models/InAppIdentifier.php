<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InAppIdentifier extends Model {

    protected $connection = 'mysql';
	protected $table = 'in_app_identifiers';

    public $timestamps = false;
}
