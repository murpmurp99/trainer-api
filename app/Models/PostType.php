<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model {

    protected $connection = 'mysql';
    protected $table = 'post_types';

    public function posts() {
        return $this->hasMany('App\Models\Post', 'post_type_id', 'id');
    }

}