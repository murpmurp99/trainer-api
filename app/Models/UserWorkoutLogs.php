<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorkoutLogs extends Model {

    protected $connection = 'mysql';
    protected $table = 'user_workout_logs';
    protected $fillable = ['user_id', 'time_spent', 'time_type', 'type_of_workout', 'workout_focus', 'experience', 'location', 'created_at', 'updated_at'];

    public $timestamps = false;

    public function userLogs()
    {
        return $this->belongsToMany('App\Models\UserLogs');
    }

    public function exerciseLogs()
    {
        return $this->hasMany('App\Models\UserExerciseLogs', 'workout_log_id', 'id');
    }

}
