<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plans extends Model {

    protected $connection = 'mysql';
    protected $table = 'plans';

    public function difficulty()
    {
        return $this->hasOne('App\Models\DifficultyLevels', 'id', 'difficulty_level_id');
    }

    public function sports()
    {
        return $this->hasOne('App\Models\SportType', 'id', 'sport_id');
    }

    public function instance()
    {
        return $this->hasMany('App\Models\PlanInstance', 'plan_alias_id', 'id');
    }

//    public function plans() {
//        return $this->hasOne('App\PlanAliases');
//    }

}
