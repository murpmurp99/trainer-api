<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Muscles extends Model {

    protected $connection = 'mysql';
	protected $table = 'muscles';

    protected $fillable = ['name', 'muscle_group_id'];

}

