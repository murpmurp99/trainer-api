<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanAliases extends Model {

    protected $connection = 'mysql';
    protected $table = 'plan_aliases';

    public function prices()
    {
        return $this->hasMany('App\Models\Prices', 'sellable_id')->where('current', '=', '1');
    }

    public function plans()
    {
        return $this->belongsTo('App\Models\Plans', 'plan_id');
    }

    public function images()
    {
        return $this->hasOne('App\Models\Images', 'attachment_id', 'id')->where('attachment_type', '=', 'PlanAlias');
    }

    public function instances()
    {
        return $this->hasMany('App\Models\PlanInstance', 'plan_alias_id', 'id');
    }

    public function exercises()
    {
        return $this->hasMany('App\Models\TrainerPlanExercises', 'training_plan_id', 'id');
    }

    public function scopeByUserId($query, $userid)
    {
        return $query->where('user_id_sort', '=', $userid);
    }

    public function scopeGetActiveOnly($query)
    {
        return $query->where('is_active', '=', '1');
    }

    public function scopeGetByGroup($query, $groupId)
    {
        return $query->where('group_id', '=', (int)$groupId);
    }

    public function scopeIsVisible($query)
    {
        return $query->where('is_visible', '=', '1');
    }

    public function scopeIsSearchable($query)
    {
        return $query->where('is_searchable', '=', '1');
    }

}
