<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MuscleGroups extends Model {

    protected $connect = 'mysql';
    protected $table = 'muscle_groups';

    protected $fillable = ['name'];

}
