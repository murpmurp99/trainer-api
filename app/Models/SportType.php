<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportType extends Model {

    protected $connection = 'mysql';
    protected $table = 'sport_types';

    public function sports() {
        return $this->belongsTo('App\Models\Plans');
    }

}
