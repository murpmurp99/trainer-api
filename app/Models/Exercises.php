<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exercises extends Model {

    protected $connection = 'mysql';
    protected $table = 'exercises_old';


    public function muscles()
    {
        return $this->hasOne('App\Models\ExerciseMuscles', 'exercise_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\ExerciseCategories', 'exercise_category_id', 'id');
    }

    public function scopeWithDescription($query)
    {
        return $query->where('user_id', '>', '');
    }

    public function scopeByCategory($query, $categoryId)
    {
        return $query->where('exercise_category_id', '=', $categoryId);
    }

}
