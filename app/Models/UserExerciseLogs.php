<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserExerciseLogs extends Model {

	protected $connection = 'mysql';
    protected $table = 'user_exercise_logs';
    protected $fillable = ['workout_log_id', 'exercise_id', 'time_spent', 'total_reps', 'total_weight', 'miles', 'pace', 'created_at', 'updated_at'];

    public $timestamps = false;

    public function exercises()
    {
        return $this->hasOne('App\Models\Exercise', 'id');
    }

    public function workoutLog()
    {
        return $this->belongsToMany('App\Models\UserWorkoutLogs', 'id');
    }

}
