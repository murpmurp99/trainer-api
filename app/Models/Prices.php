<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prices extends Model {

    protected $connection = 'mysql';
    protected $table = 'prices';

    public function prices() {
        return $this->BelongsTo('App\Models\PlanAliases');
    }

}
