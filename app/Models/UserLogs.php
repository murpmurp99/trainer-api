<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLogs extends Model {

    protected $connection = 'mysql';
    protected $table = 'user_logs';

    protected $fillable = ['user_id', 'type_of_log', 'user_date'];

    public function workoutLogs() {
        return $this->hasMany('App\Models\UserWorkoutLogs', 'user_log_id', 'id');
    }

    public function scopeByUserId($query, $userId)
    {
        return $query->where('user_id', '=', $userId);
    }

    public function scopeByUserDate($query, $date)
    {
        return $query->where('user_date', '=', $date);
    }

    public function scopeOrderByAscending($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

}
