<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanInstance extends Model {

    protected $connection = 'mysql';
    protected $table = 'plan_instances';

    public function planAlias()
    {
        return $this->hasMany('App\Models\PlanAliases', 'plan_alias_id', 'id');
    }

}
