<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachments extends Model {

	protected $connection = 'trainer';
    protected $table = 'images';

    public function scopeFindImageById($query, $id)
    {
        return $query->where('attachment_id', '=', $id);
    }

    public function scopeGetImageByType($query, $type)
    {
        return $query->where('attachment_type', '=', $type);
    }

}
