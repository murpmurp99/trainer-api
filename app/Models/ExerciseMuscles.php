<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseMuscles extends Model {

    protected $connection = 'mysql';
    protected $table = 'exercise_muscles';

    public function exercises()
    {
        return $this->belongsToMany('App\Models\Exercise', 'exercise_id', 'id');
    }

    public function muscles()
    {
        return $this->belongsTo('App\Models\Muscles', 'muscle_id', 'id');
    }

}
