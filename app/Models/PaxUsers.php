<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaxUsers extends Model {

    protected $connection = 'mysql';
    protected $table = 'pax_users';
//    protected $hidden = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}
