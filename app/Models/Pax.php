<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pax extends Model {

    protected $connection = 'mysql';
    protected $table = 'pax';

    public function paxUsers()
    {
        return $this->hasMany('App\Models\PaxUsers', 'pax_id');
    }

}
