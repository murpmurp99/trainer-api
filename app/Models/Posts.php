<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Posts extends Model {

    protected $connection = 'mysql';
    protected $table = 'posts';

    //Hidden fields
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function type()
    {
        return $this->belongsTo('App\Models\PostType', 'post_type_id', 'id');
    }

    public function pax()
    {
        return $this->belongsTo('App\Models\PostType', 'post_type_id', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function scopeGetFirstPublishedMessage($query)
    {
        return $query->where('publish_date', '=', Carbon::today()->toDateString());
    }

}
