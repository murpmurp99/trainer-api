<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class GravatarProfilePicture extends Facade {


    protected static function getFacadeAccessor()
    {
        return 'gravatarprofilepicture';
    } // most likely you want MyClass here

}