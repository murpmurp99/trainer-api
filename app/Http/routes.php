<?php

// Index Method
Route::get('/', 'IndexController@index');

// API Methods
Route::group(['prefix' => 'rest/api/v1'], function() {

    // Login Method
    Route::post('/user/login', 'LoginController@post'); // Login to AUS & Trainer

    // Forgot Password Method
    Route::post('/user/{id}/forgot', ['uses' => 'LoginController@forgotPassword']); // User forgot password method

    // User Methods
    Route::post('/account/user/create', 'UserController@createUser'); // Create a new user
    Route::put('/account/user/{id}/delete', ['uses' => 'UserController@softDeleteUser']); // Soft delete the user
    Route::put('/account/user/{id}/update', 'UserController@updateUser'); // Update existing user

    // User Goals Methods
    Route::get('/account/user/{id}/goals', 'UserController@getGoals'); // Get User Goals

    // Workout Methods
    Route::get('/user/{id}/workout/logs', ['uses' => 'UserWorkoutsController@getUserWorkoutLog']);
    Route::post('/user/create/workout/log', 'UserWorkoutsController@createUserWorkoutLog');

    // Exercise Methods
    Route::get('/exercises', 'ExercisesController@getExercises'); // Get all the available exercises
    Route::get('/exercises/categories', 'ExercisesController@getExerciseCategories'); // Get all the available exercise categories
    Route::get('/exercises/{category}/{itemReturn}', ['uses' => 'ExercisesController@fetchExercises']); // Get exercises

    Route::post('/create/exercise', 'ExercisesController@createNewUserExercise'); // Create a new exercise
    Route::put('/exercise/{id}/update', 'ExercisesController@updateUserExercise'); // Update an existing exercise
    Route::delete('/exercise/{id}/delete', 'ExercisesController@deleteUserExercise'); // Delete a user created exercise

    // Workout Methods
    Route::get('/workouts', 'WorkoutController@get'); // Get all workouts

    // Training Plans
    Route::get('/trainer/plans/{userid}/{itemReturn}', ['uses' => 'PlansController@fetchAllPlans']);
    Route::post('/trainer/plan', ['uses' => 'PlansController@startPlan']);

    // In App Identifies
    Route::get('/internal/identifiers', 'InternalController@getInAppIdentifiers');

    // Instant Workout
    Route::get('/instant/workout/{categoryId}', ['uses' => 'InstantWorkoutController@getInstantWorkout']);

    // Post Pax Message
    Route::post('/pax/message', ['uses' => 'PaxController@postMessage']);
    Route::get('/pax/{id}/messages', ['uses' => 'PaxController@getMessages']);

});

// Display all SQL executed in Eloquent
Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});

// Admin Routes
Route::get('/admin', 'Admin\AdminLoginController@index');

Route::post('/admin/login', 'Admin\AdminLoginController@login');
Route::get('/admin/logout', 'Admin\AdminLoginController@logout');

// Return Admin Panel
Route::get('/admin/panel', 'Admin\AdminPanelController@index');

// Admin User Page
Route::get('/admin/panel/users/{limit?}', ['uses' => 'Admin\AdminUsersController@get']);
Route::get('/admin/panel/user/{id}', ['uses' => 'Admin\AdminUsersController@getSingle']);

// Admin Pax Page
Route::get('/admin/panel/pax/{limit?}', ['uses' => 'Admin\AdminPaxController@index']);
Route::get('/admin/panel/pax/{id}', ['uses' => 'Admin\AdminPaxController@show']);