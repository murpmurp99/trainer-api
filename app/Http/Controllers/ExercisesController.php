<?php namespace App\Http\Controllers;

use App\Models\Exercises;
use App\Models\ExerciseCategories;

use App\Models\Attachments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExercisesController extends Controller {

    /**
     *
     * @param string $category
     * @param int $itemCount
     *
     * @return object $exercises
     *
     */
    public function fetchExercises($category, $itemCount)
    {
        $obj = new \stdClass();

        try {

            $exercisesCon = Exercises::ByCategory($category)->whereNull('user_id')->Limit($itemCount);

            $ex = array();

            $pagResults = array(
                'startIndex' => 0,
                'itemsReturned' => count($exercisesCon),
                'totalItems' => $exercisesCon->total(),
                'pageNo' => $exercisesCon->currentPage(),
                'totalPages' => $exercisesCon->lastPage()
            );

            foreach($exercisesCon as $exr) {

                $id = $exr->id;
                $type = 'Exercise';
                $image = Attachments::FindImageById($id)->GetImageByType($type)->get();
                $imagePath = '';

                foreach($image as $img) {

                    if(!empty($img)) {
                        $imagePath = 'http://trainer.active.com/uploads/images/exercise/s_' . $image[0]['image_path'];
                    } else {
                        $imagePath = 'null';
                    }

                }

                $fresh = array(
                    'exerciseId' => $exr->id,
                    'exerciseName' => $exr->name,
                    'exerciseSystemName' => $exr->system_name,
                    'exerciseDescription' => $exr->description,
                    'exerciseImageURL' => $imagePath,
                    'exerciseType' => array(),
                    'exercisesMuscles' => array()
                );

                $exercise = array(
                    'exercise' => $fresh
                );

                array_push($ex, $exercise);

            }

            $obj->paginationResult = $pagResults;
            $obj->exercises = $ex;

        } catch (\Exception $e) {

            $obj = array('databaseConnection' => 'Failed ' . $e);

        }

        return json_encode(array('exerciseList' => $obj), JSON_UNESCAPED_SLASHES);

    }


    /**
     *
     * @return object $categories
     *
     */
    public function getExercises()
    {
        $obj = new \stdClass();

        try {
            $obj = Exercises::get()
            ->sortBy(function($exercise) {
                return $exercise->exercise_type_id;
            })->toArray();

            /*->sortBy(function($exercise) {
                return $exercise->exercises_muscles.muscle_id;
            })*/



        } catch (\Exception $e) {

            $obj->Error = $e;

        }

        return json_encode($obj);
    }

    /**
     *
     * @return object $categories
     *
     */
    public function getExerciseCategories()
    {
        $obj = new \stdClass();

        try {

            $categories = ExerciseCategories::all()->toArray();
            $catArray = array();

            foreach($categories as $cg) {

                $exerciseObject = array(
                    'exerciseTypeId' => $cg['id'],
                    'exerciseTypeName' => $cg['name']
                );

                $exerciseType = array(
                    'exerciseType' => $exerciseObject
                );

                array_push($catArray, $exerciseType);

            }

            $obj->exerciseTypeList = $catArray;


        } catch (\Exception $e) {

            $obj->Error = $e;

        }

        return json_encode($obj);
    }

}
