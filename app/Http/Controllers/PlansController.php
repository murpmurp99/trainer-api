<?php namespace App\Http\Controllers;

use App\Models\Attachments;
use App\Models\PlanAliases;
use App\Models\PlanInstance;
use Illuminate\Http\Request;
use DB;
use Response;
use App\Http\Controllers\Controller;

class PlansController extends Controller {

	/**
	 * Display a listing of the resource.
	 * @param string $itemCount
	 * @return object $obj
	 */
	public function fetchAllPlans($userId, $itemCount)
	{
        $obj = new \stdClass();

        try {

            $plansCon = PlanAliases::with('plans', 'exercises.exercise', 'exercises.exercise.category', 'exercises.exercise.muscles.muscles', 'prices', 'instances', 'images', 'plans.sports', 'plans.difficulty')->GetActiveOnly()->IsVisible()->IsSearchable()->GetByGroup('21')->paginate($itemCount);

            $pl = array();

            $pagResults = array(
                'startIndex' => 0,
                'itemsReturned' => count($plansCon),
                'totalItems' => $plansCon->total(),
                'pageNo' => $plansCon->currentPage(),
                'totalPages' => $plansCon->lastPage()
            );

            foreach($plansCon as $key=>$plans) {

                $plan = array(
                    'planId' => $plans->id,
                    'planDaysCount' => $plans->plans->days,
                    'planWeekCount' => $plans->plans->weeks,
                    'planName' => $plans->name,
                    'planIsFree' => ((int)$plans->prices->toArray()[0]['price'] < 1.00 ? '0' : '1'),
                    'planPrice' => $plans->prices->toArray()[0]['price'],
                    'planHasAd' => ((int)$key === 4 ? '1' : '0' || (int)$key === 9 ? '1' : '0'),
                    'planDescription' => (!empty($plans->description) ? $plans->description : ''),
                    'planImageURL' => 'http://trainer.active.com/uploads/images/planalias/s_' . $plans->images->image_path,
                    'planCongrats' => $plans->congrats,
                    'planGoal' => $plans->goal,
                    'planAuthor' => '',
                    'planDifficultyLevel' => (!empty($plans->plans->difficulty) ? $plans->plans->difficulty->name : ''),
                    'planSportType' => array(
                        'sportId' => (!empty($plans->plans->sports) ? $plans->plans->sports->id : ''),
                        'sportName' => (!empty($plans->plans->sports) ? $plans->plans->sports->name : '')
                    ),
                    'planExercises' => [],
                    'planInstance' => []

                );

                if(!empty($plans->exercises)) {
                    foreach($plans->exercises as $exercise) {
                        $array = [
                            'planExercise' => [
                                'exerciseId' => $exercise['exercise']['id'],
                                'exerciseName' => $exercise['exercise']['name'],
                                'exerciseSystemName' => $exercise['exercise']['system_name'],
                                'exercisePoint' => $exercise['exercise']['points'],
                                'exerciseTime' => $exercise['exercise']['time'],
                                'exerciseImageURL' => '',
                                'exerciseDescription' => $exercise['exercise']['description'] !== '' ? $exercise['exercise']['description'] : '',
                                'exerciseType' => [
                                    'exerciseTypeId' => $exercise['exercise']['category']['id'],
                                    'exerciseTypeName' => $exercise['exercise']['category']['name']
                                ],
                                'exerciseMuscles' => '',
//                                'exerciseMuscles' => $exercise['exercise']['muscles']['muscles']['name']
                                'exerciseHasSets' => $exercise['exercise']['has_reps']
                            ]
                        ];

                        array_push($plan['planExercises'], $array);
                    }
                }

                if(!empty($plans->instances)) {
                    foreach($plans->instances as $instance) {
                        if((int)$instance['user_id_sort'] === (int)$userId) {
                            $plan['planInstance'] = array(
                                'planInsId' => $instance['id'],
                                'planInsUpdatedAt' => date('d/m/Y', strtotime($instance['updated_at'])),
                                'planInsProgress' => $instance['progress'],
                                'planInsStartDate' => date('d/m/Y', strtotime($instance['start_date'])),
                                'planInsEndDate' => (string)$instance['end_date'] !== '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($instance['end_date'])) : '',
                                'planInsTrainingFor' => $instance['training_for'] !== null ? $instance['training_for'] : '',
                                'planInsDaysSelected' => $instance['days_selected'],
                                'planInsIsStarted' => $instance['is_started'],
                                'planInsCompletedAt' => $instance['completed_at'] !== null ? date('d/m/Y', strtotime($instance['completed_at'])) : '',
                                'planInsShowOnDashboard' => $instance['show_on_dashboard'],
                                'planInsShowOnProfile' => $instance['show_on_profile']
                            );
                        }
                    }
                }

                $plan_s = array(
                  'plan' => $plan
                );

                array_push($pl, $plan_s);

            }

            $obj->paginationResult = $pagResults;
            $obj->plans = $pl;

        } catch (\Exception $e) {

            $obj = array('databaseConnection' => 'Failed ' . $e);

        }

        return json_encode(array('planList' => $obj), JSON_UNESCAPED_SLASHES);

    }

    public function startPlan(Request $request) {

        try {

            $instance = $request->all();
            $planInstance = new PlanInstance;

            $planInstance->user_id_sort = $instance['userId'];
            $planInstance->plan_alias_id = $instance['planId'];
            $planInstance->start_date = date('Y-m-d', strtotime($instance['planStartDate']));
            $planInstance->is_started = 1;
            $planInstance->days_selected = $instance['selectedTrainingDays'];

            $planInstance->save();

        } catch (\Exception $e) {
            return Response::json([
                'postResult' => [
                    'hasError' => '1',
                    'message' => 'Failed to save start plan.'
                ]
            ], 500);
        }

        return Response::json([
            'postResult' => [
                'hasError' => '0',
                'message' => 'Started a workout.'
            ]
        ], 200);

    }

}
