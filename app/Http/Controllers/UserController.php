<?php namespace App\Http\Controllers;

use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

//Models
use App\Models\User;

class UserController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createUser(Request $requests)
	{

        $post = $requests->all();
        $postBody = $post['user'];
        $facebookId = $postBody['fbId'];
        $facebookToken = $postBody['fbToken'];
        $emailAddress = $postBody['EmailAddress'];
        $firstName = $postBody['FirstName'];
        $lastName = $postBody['LastName'];
        $gender = $postBody['Gender'];
        $password = Hash::make($postBody['Password']);
        $birthday = new Carbon($postBody['Birthday']);

        // @TODO: Release 1.1 ask user to link accounts
//        $emailCheck = $this->checkIfUserEmailExistsAUS($emailAddress);
        $dbCheck = $this->checkIfUserExistsInDB($emailAddress);
        $now = Carbon::now();
        $userId = 0;

        if($birthday->diff($now)->days >= 4745) {
            if(!$dbCheck) {

                $user = new User;

                $user->first_name = $firstName;
                $user->last_name = $lastName;
                $user->gender = $gender;
                $user->email_address = $emailAddress;
                $user->password = $password;
                $user->facebook_id = $facebookId;
                $user->facebook_token = $facebookToken;
                $user->birthday = date('Y-m-d', strtotime($birthday));

                $user->save();
                $userId = $user->id;

            } else {

                return Response::json([
                    'postResult' => [
                        'hasError' => '1',
                        'message' => 'User already exists'
                    ]
                ], 500);

            }
        } else {
            return Response::json([
                'postResult' => [
                    'hasError' => '1',
                    'message' => 'User must be older than 13'
                ]
            ], 500);
        }

        $user = User::with('userGoalType')->where('id', '=', (string)$userId)->get();
        return Response::json([
            'message' => 'User created!',
            'user' => [
                'userId' => $user[0]['id'],
                'userEmail' => $user[0]['email_address'],
                'firstName' => $user[0]['first_name'],
                'lastName' => $user[0]['last_name'],
                'userRole' => array(
                    'userRoleId' => '2',
                    'userRoleName' => 'user',
                    'userRoleDescription' => 'Standard User'
                ),
                'onBoardInfo' => $user[0]['age'] !== null ? [
                    'onBoardAge' => $user[0]['age'] !== '' ? $user[0]['age'] : '',
                    'onBoardWeight' => $user[0]['weight'] !== '' ? $user[0]['weight'] : '',
                    'onBoardGoalCategory' => [
                        'goalCategoryId' => $user[0]['goal_type_id'] !== '' ?  $user[0]['goal_type_id'] : '',
                        'goalCategoryName' => $user[0]['userGoalType']['goal_type'] !== '' ? $user[0]['userGoalType']['goal_type'] : ''
                    ],
                    'onBoardHeightInch' => $user[0]['height'] !== '' ? $user[0]['height'] : '',
                    'onBoardGender' => $user[0]['gender'] !== '' ? $user[0]['gender'] : ''
                ] : []
            ]
        ], 200);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateUser(Request $requests, $id)
	{

        try {
            $post = $requests->all();
            $postBody = $post['user'];
            $user = User::find($id);

            $user->fill([
                'first_name' => $postBody['FirstName'],
                'last_name' => $postBody['LastName'],
                'gender' => $postBody['onBoardInfo']['onBoardGender'],
                'email_address' => $postBody['EmailAddress'],
                'facebook_id' => $postBody['fbId'],
                'facebook_token' => $postBody['fbToken'],
//                'birthday' => date('Y-m-d', strtotime($birthday)),
                'age' => $postBody['onBoardInfo']['onBoardAge'],
                'weight' => $postBody['onBoardInfo']['onBoardWeight'],
                'goal_type_id' => $postBody['onBoardInfo']['onBoardGoalCategory']['goalCategoryId'],
                'height' => $postBody['onBoardInfo']['onBoardHeightInch'],
            ]);

            $user->save();

        } catch (\Exception $e) {
            return Response::json([
                'postResult' => [
                    'hasError' => '1',
                    'message' => 'Update failed ' . $e,
                ]
            ], 200);
        }

        return Response::json([
            'postResult' => [
                'hasError' => '0',
                'message' => 'User updated!',
            ]
        ], 200);

	}

    /**
     * Determines if the email address is created in AUS as well as our DB.
     *
     * @param  string $userEmail
     * @return boolean
     */
    private function checkIfUserEmailExistsAUS($userEmail)
    {

        $wsdl = env('WSDL_KEY', '');

        $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0));

        $param = array(
            'context' => array(
                'appToken' => env('TOKEN', '')
            ),
            'userName' => (string)$userEmail
        );

        $info = $client->__soapCall("findUserByUserName", array( 'parameters' => $param));

        dd($info);

        $response = false;


        return $response;
    }

    private function checkIfUserExistsInDB($userEmail)
    {

        $rules = array('email_address' => 'unique:users,email_address');
        $validator = Validator::make(['email_address' => $userEmail], $rules);

        if($validator->fails())
        {
            $response = true;
        } else {
            $response = false;
        }

        return $response;

    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function softDeleteUser($id)
	{
        $user = User::find($id);
        $user->delete();


        return Response::json([
            'postResult' => [
                'hasError' => '0',
                'message' => 'User has been deleted.'
            ]
        ], 200);
	}


    /**
     * Restore the user
     *
     * @param $emailAddress
     * @return mixed
     */
    public function restoreUser($emailAddress)
    {
        $user = User::where('email_address', '=', $emailAddress)->get();
        $user->restore();

        return Response::json([
            'postResult' => [
                'hasError' => '0',
                'message' => 'User has been restored.'
            ]
        ], 200);
    }

}
