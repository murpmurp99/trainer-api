<?php namespace App\Http\Controllers;

use App\Models\InAppIdentifier;

use App\Http\Controllers\Controller;

class InternalController extends Controller {

    /**
     *
     * @param string $category
     * @param int $itemCount
     *
     * @return object $exercises
     *
     */
    public function getInAppIdentifiers()
    {
        $data = InAppIdentifier::get();
        
        $inAppIdentifiers = array();

        foreach ($data as $inAppIdentifier)
        {
            array_push($inAppIdentifiers, array('inAppIdentifier' => $inAppIdentifier));    
        }

        return json_encode(array('inAppIdentifiers' => $inAppIdentifiers));
    }
}
