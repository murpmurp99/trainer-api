<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\Exercises;
use App\Models\Attachments;
use Response;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class InstantWorkoutController extends Controller {

    public function getInstantWorkout($id)
    {

        $obj = new \stdClass();

        try {

            $instantWorkout = Exercises::with('category', 'muscles', 'muscles.muscles')->where('exercise_category_id', '=', $id)->orderBy(DB::raw('RAND()'))->take(5)->get();

            $exerciseArray = [];

            $pagResults = array(
                'startIndex' => 0,
                'itemsReturned' => 5,
                'totalItems' => 5,
                'pageNo' => 1,
                'totalPages' => 1
            );

            foreach($instantWorkout as $exercise) {

//                $image = Attachments::FindImageById($exercise['id'])->GetImageByType('Exercise')->get();

                $exerciseId = $exercise['id'];
                $exerciseName = $exercise['name'];
                $exerciseSystemName = $exercise['system_name'];
                $exercisePoint = (int)$exercise['points'];
                $exerciseTime = (int)$exercise['time'];
//                $exerciseImageUrl = $image->toJSON() !== '[]' ? '' : 'http://trainer.active.com/uploads/images/exercise/s_' . $image[0]['image_path'];
                $exerciseDescription = $exercise['description'];

                $ex = [
                    'exerciseId' => $exerciseId,
                    'exerciseName' => $exerciseName,
                    'exerciseSystemName' => $exerciseSystemName,
                    'exercisePoint' => (int)$exercisePoint,
                    'exerciseTime' => (int)$exerciseTime,
                    'exerciseImageUrl' => '',
                    'exerciseDescription' => $exerciseDescription,
                    'exerciseType' => [
                        'exerciseTypeId' => $exercise['exercise_category_id'],
                        'exerciseTypeName' => $exercise['category']['name']
                    ],
                    'exercisesMuscles' => '',
                    'exerciseHasSets' => (int)$exercise['has_weight'] === 0 ? 0 : 1
                ];

                array_push($exerciseArray, $ex);

                $obj->paginationResult = $pagResults;
                $obj->exercises = $exerciseArray;

            }

        } catch (\Exception $e) {

            return Response::json([
                'error' => [
                    'status' => '500',
                    'message' => 'Error occurred ' . $e
                ]
            ], 500);

        }

        return json_encode(array('exerciseList' => $obj), JSON_UNESCAPED_SLASHES);

    }

}
