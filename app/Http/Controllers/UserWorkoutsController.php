<?php namespace App\Http\Controllers;

use Response;
use Carbon\Carbon;
use App\Models\UserLogs;
use App\Models\UserWorkoutLogs;
use App\Models\UserExerciseLogs;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserWorkoutsController extends Controller {

    public function getUserWorkoutLog($userId)
    {
        try {

            $obj = new \stdClass();
            $logs = UserLogs::with('workoutLogs', 'workoutLogs.exerciseLogs')->orderBy('user_date', 'desc')->ByUserId($userId)->paginate(10);

            $pagResults = [
                'startIndex' => 0,
                'itemsReturned' => count($logs),
                'totalItems' => $logs->total(),
                'pageNo' => $logs->currentPage(),
                'totalPages' => $logs->lastPage()
            ];
            $historyLogs = [];

            foreach($logs as $l) {

                $historyLog = [
                    'historyLog' => [
                        'historyDate' => date("m/d/Y", strtotime($l['user_date'])),
                        'userId' => $l['user_id'],
                        'historyLogDetails' => []
                    ]
                ];

                $workouts = $l['workoutLogs'];

                foreach($workouts as $workoutLogs) {

                    $calories = 0;
                    $timeSpent = 0;
                    $steps = 0;

                    $historyLogDetail = [
                        'historyLogDetail' => [
                            'historyTimeSpent' => '',
                            'historyTimeType' => 'mins',
                            'historyLogIcon' => $workoutLogs['workout_icon'],
                            'historyType' => $workoutLogs['workout_type'] !== '' ? $workoutLogs['workout_type'] : '',
                            'historyTotalCalorieValue' => $calories,
                            'historyTotalMileValue' => $steps,
                            'historyFocusOrSteps' => $workoutLogs['workout_focus'],
                            'historyFeelingFace' => $workoutLogs['experience'],
                            'historyLocation' => $workoutLogs['location'],
                            'historyExerciseLogs' => []
                        ]
                    ];

                    $exerciseLogs = $workoutLogs['exerciseLogs'];

                    foreach($exerciseLogs as $exercises) {

                        $calories+= $exercises['calories_burned'];
                        $timeSpent+= $exercises['time_spent'];
                        $steps+= $exercises['miles'];

                        $exerciseLog = [
                            'exerciseLog' => [
                                'exerciseId' => 1,
                                'exerciseName' => 'Running',
                                'exerciseTime' => (int)$exercises['time_spent'],
                                'exerciseMiles' => (int)$exercises['miles'],
                                'exerciseCalories' => (int)$exercises['calories_burned'],
                                'exerciseLogSets' => ($exercises['total_reps'] === '' ? '' : [])
                            ]
                        ];

                        if($exercises['total_reps'] !== '') {

                        }

                        array_push($historyLogDetail['historyLogDetail']['historyExerciseLogs'], $exerciseLog);
                    }

                    $historyLogDetail['historyLogDetail']['historyTotalCalorieValue'] = (int)$calories;
                    $historyLogDetail['historyLogDetail']['historyTotalMileValue'] = (int)$steps;
                    $historyLogDetail['historyLogDetail']['historyTimeSpent'] = (int)$timeSpent;
                    array_push($historyLog['historyLog']['historyLogDetails'], $historyLogDetail);
                }

                array_push($historyLogs, $historyLog);
            }

            $obj->paginationResult = $pagResults;
            $obj->historyLogs = $historyLogs;

        } catch (\Exception $e) {

            return Response::json([
                'postResult' => [
                    'hasError' => '1',
                    'message' => 'Failed to get workout logs!'
                ]
            ], 500);

        }

        return json_encode(array('historyLogList' => $obj), JSON_UNESCAPED_SLASHES);
    }

    public function createUserWorkoutLog(Request $request)
    {
        $all = $request->all();
        $userId = $all['userId'];

        try {

            $date = date('Y-m-d', strtotime($all['historyDate']));
            $checkLog = UserLogs::ByUserId($userId)->ByUserDate($date)->get();

            if($checkLog->toJSON() === '[]') {

                $userLog = new UserLogs;
                $userLog->user_id = $userId;
                $userLog->type_of_log = 'workout';
                $userLog->user_date = date('Y-m-d', strtotime($all['historyDate']));

                $userLog->save();

                $logID = $userLog->id;

            } else {

                $logID = $checkLog[0]['id'];

            }

            $details = $request['historyLogDetails'];
            $workoutLog = new UserWorkoutLogs;

            foreach($details as $log) {

                $workoutLog->user_log_id = $logID;
                $workoutLog->user_id = $userId;
                $workoutLog->workout_type = $log['historyType'] !== '' ? $log['historyType'] : '';
                $workoutLog->workout_icon = $log['historyLogIcon'] !== '' ? $log['historyLogIcon'] : '';
                $workoutLog->time_spent = $log['historyTimeSpent'] !== '' ? $log['historyTimeSpent'] : '';
                $workoutLog->time_type = $log['historyTimeType'] !== '' ? $log['historyTimeType'] : '';
                $workoutLog->workout_focus = $log['historyFocusOrSteps'] !== '' ? $log['historyFocusOrSteps'] : '';
                $workoutLog->experience = $log['historyFeelingFace'] !== '' ? $log['historyFeelingFace'] : '';
                $workoutLog->location = $log['historyLocation'] !== '' ? $log['historyLocation'] : '';

                $workoutLog->save();

                $workoutLogId = $workoutLog->id;

                $exercisesDetails = $log['historyExerciseLogs'];

                foreach($exercisesDetails as $exercise) {

                    $exerciseId = $exercise['exerciseId'] !== '' ? $exercise['exerciseId'] : '';
                    $exerciseTime = $exercise['exerciseTime'] !== '' ? $exercise['exerciseTime'] : '';
                    $exerciseMile = $exercise['exerciseMiles'] !== '' ? $exercise['exerciseMiles'] : '';
                    $exerciseCalories = $exercise['exerciseCalories'] !== '' ? $exercise['exerciseCalories'] : '';

                    if(!empty($exercise['exerciseLogSets'])) {
                        $exerciseSets = $exercise['exerciseLogSets'] !== '' ? $exercise['exerciseLogSets'] : '';
                        foreach($exerciseSets as $sets) {
                            UserExerciseLogs::create([
                                'workout_log_id' => $workoutLogId,
                                'exercise_id' => $exerciseId,
                                'time_spent' => $exerciseTime,
                                'miles' => $exerciseMile,
                                'total_reps' => $sets['repCount'],
                                'total_weight' => $sets['weight'],
                                'calories_burned' => $exerciseCalories,
                            ]);
                        }
                    } else {
                        UserExerciseLogs::create([
                            'workout_log_id' => $workoutLogId,
                            'exercise_id' => $exerciseId,
                            'time_spent' => $exerciseTime,
                            'miles' => $exerciseMile,
                            'calories_burned' => $exerciseCalories,
                        ]);
                    }
                }
            }
        } catch (\Exception $e) {

            dd($e);

            return Response::json([
                'postResult' => [
                    'hasError' => '1',
                    'message' => 'Failed to save workout log!'
                ]
            ], 500);

        }

        return Response::json([
            'postResult' => [
                'hasError' => '0',
                'message' => 'Workout Log Saved!'
            ]
        ], 400);
    }

}
