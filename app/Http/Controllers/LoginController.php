<?php namespace App\Http\Controllers;

use Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;

//Models
use App\Models\User;

class LoginController extends Controller
{

    /**
     * The POST method that instantiate
     *
     * @param $request
     * @return object userObject
     *
     * return $userObject - Which contains a JSON
     * object of the current request user
     */
    public function post(Request $request)
    {

        $postBody = $request->input('user');
        $userName = $postBody['EmailAddress'];
        $userPass = $postBody['Password'];

        $response = $this->checkIfUserExists($userName);

        if (!empty($response)) {

//            $activeXDB = $this->checkIfUserExistsInActiveXDB($userName, $userPass);
            return $this->getUserName($userName, $userPass);

//            return $activeXDB;

        } else {

            return $this->getUserName($userName, $userPass);

        }

    }


    protected function buildUserObject($user, $password)
    {

        try {

            $rules = array('email_address' => 'unique:users,email_address');
            $validator = Validator::make(['email_address' => $user->return->userName], $rules);
            $return = $user->return;

            $userId = 0;

            if (!$validator->fails()) {
                $userModel = new User;

                $userModel->aus_id = $return->id;
                $userModel->first_name = $return->firstName;
                $userModel->last_name = $return->lastName;
                $userModel->gender = (isset($return->gender) ? $return->gender : '');
                $userModel->email_address = $return->userName;
                $userModel->password = Hash::make($password);
                $userModel->facebook_id = '';
                $userModel->facebook_token = '';
                $userModel->birthday = date('Y-m-d', strtotime(!empty($return->dob) ? $return->dob : ''));

                $userModel->save();
                $userId = $userModel->id;
            } else {
                $userData = User::with('userGoalType')->where('email_address', '=', $user->return->userName)->get();
                $onBoard = $userData;
                $userId = $onBoard[0]['id'];
            }

            if(!empty($onBoard[0]['weight'])) {
                $object = array(
                    'userId' => $userId,
                    'userGender' => (isset($return->gender) ? $return->gender : ''),
                    'userEmail' => $user->return->userName,
                    'firstName' => $user->return->firstName,
                    'lastName' => $user->return->lastName,
                    'userRole' => array(
                        'userRoleId' => '2',
                        'userRoleName' => 'user',
                        'userRoleDescription' => 'Standard User'
                    ),
                    'userAdmin' => array(
                        'isAdmin' => $onBoard[0]['admin']
                    ),
                    'onBoardInfo' => $onBoard[0]['age'] !== null ? [
                        'onBoardAge' => $onBoard[0]['age'],
                        'onBoardWeight' => $onBoard[0]['weight'],
                        'onBoardGoalCategory' => [
                            'goalCategoryId' => $onBoard[0]['goal_type_id'],
                            'goalCategoryName' => $onBoard[0]['userGoalType']['goal_type']
                        ],
                        'onBoardHeightInch' => $onBoard[0]['height'],
                        'onBoardGender' => $onBoard[0]['gender']
                    ] : ''
                );
            } else {
                $object = array(
                    'userId' => $userId,
                    'userGender' => (isset($return->gender) ? $return->gender : ''),
                    'userEmail' => $user->return->userName,
                    'firstName' => $user->return->firstName,
                    'lastName' => $user->return->lastName,
                    'userRole' => array(
                        'userRoleId' => '2',
                        'userRoleName' => 'user',
                        'userRoleDescription' => 'Standard User'
                    ),
                    'userAdmin' => array(
                        'isAdmin' => $onBoard[0]['admin']
                    ),
                    'onBoardInfo' => []
                );
            }


        } catch (\Exception $e) {

            dd($e);

            $object = array(
                'userEmail' => 'Not Found'
            );

        }

        return json_encode(array('user' => $object), JSON_FORCE_OBJECT);

    }

    protected function checkIfUserExists($userName)
    {

        $wsdl = env('WSDL_KEY', '');
        $token = env('TOKEN', '');

        $param = array(
            'context' => array(
                'appToken' => $token
            ),
            'userName' => $userName,
        );

        $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0));
        $info = $client->__soapCall("doesUserNameExist", array('parameters' => $param));

        if (!$info->return) {
            return Response::json([
                'error' => [
                    'status' => '404',
                    'message' => 'Username Does not exists in AUS'
                ]
            ], 404);
        }

    }

    protected function validatePassword($data, $password)
    {

        $wsdl = env('WSDL_KEY', '');
        $token = env('TOKEN', '');

        $param = array(
            'context' => array(
                'appToken' => $token
            ),
            'userId' => $data->return->id,
            'currentPassword' => $password,
            'newPassword' => $password
        );

        $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0));
        $info = $client->__soapCall("setPassword", array('parameters' => $param));


        if (!empty($info->faultcode)) {

            return Response::json([
                'error' => [
                    'status' => '400',
                    'message' => 'Password is not valid'
                ]
            ], 400);

        } else {

            return $this->buildUserObject($data, $password);

        }

    }

    protected function checkIfUserExistsInActiveXDB($username, $password)
    {

        try {

            $user = User::with('userGoalType')->where('email_address', '=', $username)->get();
            $passwordMD5 = md5($password);

//            dd(Hash::check('' . (string)$passwordMD5, (string)$user[0]['password']));

            if(Hash::check('' . (string)$passwordMD5 . '', (string)$user[0]['password']))
            {
                $object = array(
                    'userId' => $user[0]['id'],
                    'userEmail' => $user[0]['email_address'],
                    'firstName' => $user[0]['first_name'],
                    'lastName' => $user[0]['last_name'],
                    'userRole' => array(
                        'userRoleId' => '2',
                        'userRoleName' => 'user',
                        'userRoleDescription' => 'Standard User'
                    ),
                    'onBoardInfo' => $user[0]['age'] !== null ? [
                        'onBoardAge' => $user[0]['age'] !== '' ? $user[0]['age'] : '',
                        'onBoardWeight' => $user[0]['weight'] !== '' ? $user[0]['weight'] : '',
                        'onBoardGoalCategory' => [
                            'goalCategoryId' => $user[0]['goal_type_id'] !== '' ?  $user[0]['goal_type_id'] : '',
                            'goalCategoryName' => $user[0]['userGoalType']['goal_type'] !== '' ? $user[0]['userGoalType']['goal_type'] : ''
                        ],
                        'onBoardHeightInch' => $user[0]['height'] !== '' ? $user[0]['height'] : '',
                        'onBoardGender' => $user[0]['gender'] !== '' ? $user[0]['gender'] : ''
                    ] : []
                );
            } else {
                return Response::json([
                    'postResult' => [
                        'hasError' => '1',
                        'message' => 'Password is not valid.'
                    ]
                ], 400);
            }

        } catch (\Exception $e) {

            $object = array(
                'userEmail' => 'Not Found'
            );

        }

        return json_encode(array('user' => $object), JSON_FORCE_OBJECT);

    }

    protected function getUserName($username, $password)
    {

        $wsdl = env('WSDL_KEY', '');

        $client = new \SoapClient($wsdl, array("trace" => 1, "exceptions" => 0));

        $param = array(
            'context' => array(
                'appToken' => env('TOKEN', '')
            ),
            'userName' => (string)$username
        );

        $info = $client->__soapCall("findUserByUserName", array('parameters' => $param));

        $password = $this->validatePassword($info, $password);

        return $password;

    }

}