<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Models\User;
use Auth;
use App\Http\Controllers\Controller;

class AdminUsersController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of all the resources in the Users table
	 *
	 * @return Response
	 */
	public function get($userlimit = 15)
	{
		// Return users from model with a limit & pagination
        $users = User::paginate($userlimit);

		$user = Auth::user();
        return view('admin.admin-users')->with('users', $users)->with('user', $user);
	}

	/**
	 * Fetch a single user record from the DB containing all the fields.
	 *
	 * @return Response
	 */
	public function getSingle($userId)
	{
		$user = User::where('id', $userId)->first();
		return view('admin.admin-home')->with('user', $user);
	}

	/**
	 * Update the user record in the db.
	 *
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function delete($id)
	{
		//
	}

    /**
     * Disable a user from accessing the app
     *
     * @param $id
     * @return Response
     */
    public function disable($id)
    {

    }

}
