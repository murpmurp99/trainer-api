<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Models\User;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AdminLoginController extends Controller {

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login()
    {

        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('admin')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                $user = User::where('email', $userdata['email'])->first();

                if($user->admin == 1) {

                    // Validation Successful
                    return Redirect::to('/admin/panel');

                } else {

                    // User is not an admin
                    return Redirect::to('admin')
                        ->withErrors($userdata['email'] . " is not an admin.") // send back all errors to the login form
                        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form;
                }

            } else {

                // validation not successful, send back to form
                return Redirect::to('admin');

            }

        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('admin');
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function get($username, $password)
    {

        $passwordMD5 = md5($password);

        $user = User::where('email_address', '=', $username)->where('admin', '=', '1')->first();

        if(count($user) > 0) {
            if(Hash::check('' . (string)$passwordMD5 . '', (string)$user['password'])) {
//                return json_encode(array('User' => $user), JSON_FORCE_OBJECT);
                return response()->json(array('User' => $user))->setCallback();
            } else {
                return response()->json(['User' => ['errorMessage' => 'Username or password is incorrect please try again.']], 200)->setCallback();
            }
        } else {
            return response()->json(['User' => ['errorMessage' => 'Username or password is incorrect please try again.']], 200)->setCallback();
        }

    }

}
