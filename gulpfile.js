var elixir = require('laravel-elixir');
require("laravel-elixir-uglify");

elixir.config.sourcemaps = false;
//elixir.config.registerWatcher("uglify", "resources/assets/js/**/*.js");

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.
        scripts('**/*.js')

        .uglify('**/*.js', 'public/js', {
            mangle: true,
            suffix: '.min'
        })

        .sass('admin.scss', 'public/css/admin.css')

        .version(["css/admin.css", 'js/all.js']);

});