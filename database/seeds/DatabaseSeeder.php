<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

// Models
use App\Models\MuscleGroups;
use App\Models\UserLogs;
use App\Models\UserWorkoutLogs;
use App\Models\UserExerciseLogs;
use App\Models\InAppIdentifier;
use App\Models\UserGoalTypes;
use App\Models\SportType;
use App\Models\DifficultyLevels;
use App\Models\Muscles;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('MuscleGroupsSeeder');
        $this->call('MusclesSeeder');
        $this->command->info('Muscles seeded!');

        $this->call('SeedUserGoalTypes');
        $this->command->info('Goal Types seeded!');

        $this->call('SportTypeSeeder');
        $this->command->info('Sport Types seeded!');

        $this->call('DifficultyLevelSeeder');
        $this->command->info('Difficulty Levels seeded!');

		$this->call('InAppIdentifierSeeder');
        $this->command->info('In App Identifiers seeded!');
	}

}

class MuscleGroupsSeeder extends Seeder {

    public function run()
    {

        $muscleGroups = ['Abs', 'Arms', 'Back', 'Chest', 'Legs', 'Shoulders'];

        for($i = 0; $i < count($muscleGroups); $i++) {
            MuscleGroups::create(['name' => $muscleGroups[$i]]);
        }
    }

}

class MusclesSeeder extends Seeder {


    public function run()
    {

        $muscleSubGroups = [
            'Obliques',
            'Lower Abs',
            'Upper Abs',
            'Biceps',
            'Triceps',
            'Forearms',
            'Upper Chest',
            'Full Chest',
            'Lower Chest',
            'Traps',
            'Deltoids',
            'Lats',
            'Delts',
            'Upper Back',
            'Lower Back',
            'Quads',
            'Hamstrings',
            'Glutes',
            'Calves'
        ];
        $musclesGroupId = [
            '1',
            '1',
            '1',
            '2',
            '2',
            '2',
            '4',
            '4',
            '4',
            '6',
            '6',
            '3',
            '3',
            '3',
            '3',
            '5',
            '5',
            '5',
            '5'
        ];

        for($i = 0; $i < count($muscleSubGroups); $i++) {
            Muscles::create(['name' => $muscleSubGroups[$i], 'muscle_group_id' => $musclesGroupId[$i]]);
        }

    }

}


class ExerciseHistoryLog extends Seeder {

    public function run()
    {

        for($i = 0; $i < 4; $i++) {
            UserLogs::create(['user_id' => '' . ($i + 1) . '', 'log_type' => 'workout', 'join_id' => '' . ($i + 1) . '']);
            UserWorkoutLogs::create(['exercise_log_id' => '' . ($i + 1) . '', 'time_spent' => '' . (5 * ($i + 1)) . '', 'time_type' => 'mins', 'type_of_workout' => 'cardio', 'workout_focus' => '', 'experience' => 'enjoyable', 'location' => 'home']);
        }

        for($k = 0; $k < 4; $k++) {
            UserExerciseLogs::create(['workout_log_id' => '1', 'exercise_id' => '1', 'time_spent' => '' . 5 * 4 . '', 'total_reps' => '', 'total_weight' => '', 'miles' => '5', 'pace' => '1.4']);
            UserExerciseLogs::create(['workout_log_id' => '2', 'exercise_id' => '2', 'time_spent' => '' . 5 * 4 . '', 'total_reps' => '', 'total_weight' => '', 'miles' => '5', 'pace' => '1.4']);
            UserExerciseLogs::create(['workout_log_id' => '3', 'exercise_id' => '3', 'time_spent' => '' . 5 * 4 . '', 'total_reps' => '', 'total_weight' => '', 'miles' => '5', 'pace' => '1.4']);
            UserExerciseLogs::create(['workout_log_id' => '4', 'exercise_id' => '4', 'time_spent' => '' . 5 * 4 . '', 'total_reps' => '', 'total_weight' => '', 'miles' => '5', 'pace' => '1.4']);
        }
    }

}

class InAppIdentifierSeeder extends Seeder {

    public function run()
    {
        //DB::table('in_app_idetifier')->delete();

        InAppIdentifier::create([
            'identifier' => 'monthlypurchase',
            'description' => 'Monthly Purchase',
            'price' => '19.99',
        ]);

        InAppIdentifier::create([
            'identifier' => 'yearlypurchase',
            'description' => 'Yearly Purchase',
            'price' => '49.99',
        ]);
    }

}

class SeedUserGoalTypes extends Seeder {

    public function run()
    {

        UserGoalTypes::create(['goal_type' => 'Weight Loss']);
        UserGoalTypes::create(['goal_type' => 'Get Active']);
        UserGoalTypes::create(['goal_type' => 'Health & Wellness']);
        UserGoalTypes::create(['goal_type' => 'Train for an Event']);
    }

}

class SportTypeSeeder extends Seeder {

    public function run()
    {

        $sportTypes = ['Running', 'Strength Training', 'Weight Loss', 'Triathlon', 'Cycling', 'Walking'];
        $sportTypeSysName = ['running', 'strength-training', 'weight-loss', 'triathlon', 'cycling', 'walking'];

        foreach($sportTypes as $key=>$sport) {
            SportType::create(['name' => $sport, 'system_name' => $sportTypeSysName[$key]]);
        }

    }

}

class DifficultyLevelSeeder extends Seeder {

    public function run()
    {

        $difficultyLevel = ['Beginner', 'Intermediate', 'Advanced'];

        foreach($difficultyLevel as $level) {
            DifficultyLevels::create(['name' => $level, 'system_name' => strtolower($level)]);
        }

    }

}