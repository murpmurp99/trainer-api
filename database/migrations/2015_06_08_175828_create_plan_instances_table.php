<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanInstancesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_instances', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id_sort');
            $table->double('progress', 2);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->char('training_for')->nullable();
            $table->char('days_selected');
            $table->integer('is_started');
            $table->integer('plan_alias_id');
            $table->integer('is_pending')->default('0');
            $table->timestamp('completed_at')->nullable();
            $table->integer('show_on_dashboard')->default('1');
            $table->integer('show_on_profile')->default('1');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_instances');
	}

}
