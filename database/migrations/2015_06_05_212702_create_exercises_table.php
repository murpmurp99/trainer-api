<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exercises', function(Blueprint $table)
		{
			$table->increments('id');
            $table->char('name');
            $table->char('description')->nullable();
            $table->integer('exercise_categories_id');
            $table->integer('met');
            $table->tinyInteger('has_duration');
            $table->tinyInteger('has_distance');
            $table->tinyInteger('has_pace');
            $table->tinyInteger('has_average_heartrate');
            $table->tinyInteger('has_target_heartrate');
            $table->tinyInteger('has_grade');
            $table->tinyInteger('has_watts');
            $table->tinyInteger('has_intensity');
            $table->tinyInteger('has_altitude_gained');
            $table->tinyInteger('has_reps');
            $table->tinyInteger('has_calories');
            $table->integer('user_id');
            $table->tinyInteger('is_searchable');
            $table->tinyInteger('is_workoutable');
            $table->tinyInteger('is_planable');
            $table->tinyInteger('mileage_loggable');
            $table->char('system_name');
            $table->tinyInteger('has_steps');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exercises');
	}

}
