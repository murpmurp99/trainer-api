<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->tinyInteger('active');
            $table->tinyInteger('admin');
            $table->char('aus_id')->nullable();
            $table->char('facebook_id')->nullable();
            $table->char('facebook_token')->nullable();
            $table->char('first_name');
            $table->char('last_name');
            $table->char('gender');
            $table->char('email_address');
            $table->char('password');
            $table->integer('age')->nullable();
            $table->timestamp('birthday');
            $table->integer('height')->nullable();
            $table->integer('weight')->nullable();
            $table->char('google2fa_secret')->nullable();
            $table->tinyInteger('two_factor');
            $table->integer('goal_type_id')->nullable();
            $table->boolean("type_of_account")->default(false);
            $table->rememberToken()->nullable();
			$table->timestamps();
            $table->softDeletes();

            $table->unique('email_address');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
