<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plans', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id');
            $table->integer('days');
            $table->integer('weeks');
            $table->char('name');
            $table->longText('description');
            $table->longText('congrats');
            $table->integer('theme_id')->nullable();
            $table->char('goal');
            $table->integer('difficulty_level_id')->nullable();
            $table->integer('sport_id')->nullable();
            $table->char('tag_name');
            $table->integer('behaviour_id')->nullable();
            $table->char('uuid');
            $table->integer('plan_distance_id')->nullable();
            $table->char('scheduled_days')->nullable();
            $table->integer('group_id')->nullable();
            $table->integer('achievement_id')->nullable();
            $table->integer('author_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plans');
	}

}
