<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWorkoutLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_workout_logs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_log_id');
            $table->char('user_id');
            $table->char('workout_icon')->nullable();
            $table->char('workout_type')->nullable();
            $table->integer('time_spent')->nullable();
            $table->char('time_type')->nullable();
            $table->char('workout_focus')->nullable();
            $table->char('experience')->nullable();
            $table->char('location')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_workout_logs');
	}

}
