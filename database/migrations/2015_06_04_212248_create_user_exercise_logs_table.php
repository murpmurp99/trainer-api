<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExerciseLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_exercise_logs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('workout_log_id');
            $table->integer('exercise_id');
            $table->double('time_spent', 1)->nullable();
            $table->integer('total_reps')->nullable();
            $table->integer('total_weight')->nullable();
            $table->double('miles', 4)->nullable();
            $table->double('pace', 3)->nullable();
            $table->integer('calories_burned');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_exercise_logs');
	}

}
